function d = d2( radius, p1x, p1y, p2x, p2y )
    % implementation of (2b)
    % p1 = (p1x,p1y), p2 = (p2x,p2y)
    d = radius * deg2rad(acosd(sind(p1x)*sind(p2x)+cosd(p1x)*cosd(p2x)*cosd(p2y-p1y)));
end

