radius = 6371.0088; % radius of earth in km
p1x = 49.261245;
p1y = -123.248109; % p1 = (49.261245,-123.248109)
p2x = 49.266772;
p2y = -123.249843; % p2 = (49.266772,-123.249843)

D1 = d1(radius,p1x,p1y,p2x,p2y); % calculate distance between p1 and p2
D2 = d2(radius,p1x,p1y,p2x,p2y); % calculate same distance using second formula

disp(D1); % print result of d1
disp(D2); % print result of d2
