theta = logspace(-12,0,13); % vector of logarithmically spaced values

A = zeros(13,3); % matrix A containing theta values and results of H1 and H2
A(:,1) = theta; % record theta values to matrix under the first column

for i = 1 : 13
    A(i,2) = H1(A(i,1)); % record results of H1(theta) to matrix under the second column
end

for i = 1 : 13
    A(i,3) = H2(A(i,1)); % record results of H2(theta) to matrix under the third column
end

header = 'Theta ... H1(Theta) ... H2(Theta)'; % header for matrix table
disp(header); % print table header 
disp(A) % print matrix