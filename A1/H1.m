function f = H1(theta)
    % implementation of (1a)
    f = (sind(theta/2))^2; %theta in degrees
end

