function f = H2(theta)
    % implementation of (1b)
    f = 0.5*(1-cosd(theta)); % theta in degrees
end

