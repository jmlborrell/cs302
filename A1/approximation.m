x0 = 1.2;
fp = cos(x0); % precise derivative at x0
i = -20:0.5:0;
h = 10.^i;
f = (sin(x0+h) - sin(x0-h))./(2*h); % approximation of derivative at x0
err = abs(fp - f); % absolute error between approxiamation and precise derivative
d_err = fp * (h.^2/6); % expected error (h^/6)*third derivative at x0
    
loglog(h,err,'-*'); % plot absolute errors
hold on
loglog(h,d_err,'r-.'); % plot expected error
xlabel('h');
ylabel('Absolute Error');