haversine; % run script and create matrix with theta values and results of H1 and H2

theta = A(:,1); % create vector with theta values from matrix
h1 = A(:,2); % create vector with results of H1(theta) from matrix
h2 = A(:,3); % create vector with results of H1(theta) from matrix

absoluteErrorVector = arrayfun(@abs,h1 - h2); % create vector with absolute errors
relativeErrorVector = zeros(1,13); % create empty vector to store relative errors

for i = 1 : 13
    relativeErrorVector(1,i) = abs(h1(i)-h2(i))/abs(h1(i)); % store relative errors to vector
end

loglog(theta,absoluteErrorVector,'-*'); % plot absolute errors
hold on
loglog(theta,relativeErrorVector,'r-.'); % plot relative errors

legend('Absolute Error', 'Relative Error'); % create legend for plot

xlabel('Theta');
ylabel('Error');