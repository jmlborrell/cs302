function d = d1( radius, p1x, p1y, p2x, p2y )
    % implementation of (2a)
    % p1 = (p1x,p1y), p2 = (p2x,p2y)
    d = 2 * radius * deg2rad(asind(sqrt(H1(p2x-p1x)+cos(p1x)*cos(p2x)*H1(p2y-p1y))));
end

