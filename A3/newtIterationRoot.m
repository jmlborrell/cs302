function root = newtIterationRoot( a, p, init )

% Function definition for question (3a)

format('long','g');

f = @(x) x^p - a; % Defintion of f(x)
f_dev = @(x) p*x^(p-1); % Definition of f'(x) (i.e. the first derivative of f(x))

i = 1; % Counter
x_k(i) = init; % Set initial guess to init
error(i) = 1; % Set initial error
k(i) = 0; % Start at 0th iteration
f_x(i) = f(init); % Set initial f(x_0);

while ( error(i) > 1e-8 ) % Loop until error < atol
    
    x_k(i+1) = x_k(i) - (f(x_k(i)) / f_dev(x_k(i))); % Calculate next iterate
    error(i+1) = abs(x_k(i+1)-x_k(i)); % Calculate current error
    f_x(i+1) = f(x_k(i+1)); % Record f(x_k+1)
    k(i+1) = i; % Update current iteration count
    i = i+1; % Increment counter
     
end

root = x_k(i); % root = final iterate

k = k.'; % Transpose the vector containing all the calculated x_k
x_k = x_k.'; % Transpose the vector containing the iteration counter
f_x = f_x.'; % Transpose the vector containing all the calculated f(x_k)

T = table(k,x_k,f_x); % Create table with k, x_k, and f(x_k) as the three columns
disp(T); % Print the table

end

