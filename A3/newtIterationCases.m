% Generates tables of both cases in question (2b)

% Calculate and display table of x_k for case x_0 = 3.0
disp('x_0 = 0.3');
a(1) = newtIteration(0.3);

% Calculate and display table of x_k for case x_0 = 1.0
disp('x_0 = 1.0');
a(2) = newtIteration(1.0);