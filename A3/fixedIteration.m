function root = fixedIteration()

% Function definition for question (2a)

format('long','g');

i = 1; % Counter
x(i) = 0.5; % Set initial guess
error(i) = 1; % Set initial error

g = @(x) ((1-x(i))*(3+x(i))^0.5)/(3.06*(1+x(i))^0.5); % g(x) definition

while ( error(i) > 1e-6 ) % Loop until error < atol
    
    x(i+1) = g(x(i)); % Calculate next iterate
    error(i+1) = abs(x(i+1)-x(i)); % Calculate current error
    i = i+1; % Increase counter
    
end

root = x(i); % root = final iterate 
disp(root); % print root

end

