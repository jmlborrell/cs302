% Generates tables of both cases in question (3c)

a = 2*exp(1); % Set a = 2e^1

% Calculate and display table of x_k for case x_0 = 0.1
disp('x_0 = 0.1');
r(1) = newtIterationReciprocal(a,0.1);

% Calculate and display table of x_k for case x_0 = 3.0
disp('x_0 = 3.0');
r(2) = newtIterationReciprocal(a,3.0);