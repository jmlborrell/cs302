function root = newtIteration(init)

% Function definition for question (2b)

format('long','g');

i = 1; % Counter
x_k(i) = init; % Set initial guess to init
error(i) = 1; % Set initial error
k(i) = 0; % Start at 0th iteration

f = @(x) ((1-x(i))*(3+x(i))^0.5)/(3.06*(1+x(i))^0.5) - x; % Defintion of f(x)
f_dev = @(x) (-1*x^2-3*x-4)/(3.06*(1+x)^0.5) - 1; % Definition of f'(x) (i.e. the first derivative of f(x))

while ( error(i) > 1e-10 ) % Loop until error < atol
    
    x_k(i+1) = x_k(i) - (f(x_k(i)) / f_dev(x_k(i))); % Calculate next iterate
    error(i+1) = abs(x_k(i+1)-x_k(i)); % Calculate current error
    k(i+1) = i; % Update current iteration count
    i = i+1; % Increment counter
    
end

root = x_k(i); % root = final iterate

x_k = x_k.'; % Transpose the vector containing all the calculated x_k
k = k.'; % Transpose the vector containing the iteration counter

T1 = table(k,x_k); % Create table with iteration count in first column and corresponding x_k in the second column
disp(T1); % Print the table

end

