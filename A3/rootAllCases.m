% Generates tables of all six cases in question (3a)

% Calculate and display table of x_k for case (a,p) = (0,2) with x_0 = 0.1
disp('(a,p) = (0,2)');
a(1) = newtIterationRoot(0,2,0.1);

% Calculate and display table of x_k for case (a,p) = (0,3) with x_0 = 0.1
disp('(a,p) = (0,3)');
a(2) = newtIterationRoot(0,3,0.1);

% Calculate and display table of x_k for case (a,p) = (10,2) with x_0 = 3.5
disp('(a,p) = (10,2)');
a(3) = newtIterationRoot(10,2,3.5);

% Calculate and display table of x_k for case (a,p) = (10,3) with x_0 = 2.1
disp('(a,p) = (10,3)');
a(4) = newtIterationRoot(10,3,2.1);

% Calculate and display table of x_k for case (a,p) = (100,2) with x_0 = 10.5
disp('(a,p) = (100,2)');
a(5) = newtIterationRoot(100,2,10.5);

% Calculate and display table of x_k for case (a,p) = (100,3) with x_0 = 4.5
disp('(a,p) = (100,3)');
a(6) = newtIterationRoot(100,3,4.5);