% Definition of f_1(x)
first  = @(x) x*(x*(x*(x*(x*(x*(x*(x*(x-18)+144)-672)+2016)-4032)+5376)-4608)+2304)-512;

% Definition of f_2(x)
second = @(x) (x-2)^9;

% Definition of f_3(x)
third  = @(x) x^9-18*x^8+144*x^7-672*x^6+2016*x^5-4032*x^4+5376*x^3-4608*x^2+2304*x-512;

i = 1.94:0.001:2.08; % Range of equdistant samples

firstFunc  = arrayfun(first ,i); % map f_1(x) to samples
secondFunc = arrayfun(second,i); % map f_2(x) to samples
thirdFunc  = arrayfun(third ,i); % map f_3(x) to samples

% Plot f_1(x)
figure(1)
plot(i,firstFunc,'-*');
title('x(x(x(x(x(x(x(x(x-18)+144)-672)+2016)-4032)+5376)-4608)+2304)-512')
xlabel('x');
ylabel('f(x)');

% Plot f_2(x)
figure(2)
plot(i,secondFunc,'-*');
title('(x-2)^9');
xlabel('x');
ylabel('f(x)');

% Plot f_3(x)
figure(3)
plot(i,thirdFunc,'-*');
title('x^9-18x^8+144x^7-672x^6+2016x^5-4032x^4+5376x^3-4608x^2+2304x-512')
xlabel('x');
ylabel('f(x)');