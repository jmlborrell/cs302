function [ x ] = pentadiagonal( ld2, ld1, cd, ud1, ud2, b)
%PENTADIAGONAL Summary of this function goes here
%   This function solves a pentadiagonal system of linear equations
%   The input of the diagonals is from the lowest subdiagonal to the
%   highest superdiagonal (from bottom to top) IN THAT ORDER

n = length(cd); % Calculate length of the matrix
x = zeros(n,1); % Create vector to store resulting x


% This is the algorithm to calculate the L and U components of the 
% pentadiagonal matrix. To save space, the six vectors are simply
% overwritten with the corresponding L and U components keeping the space
% complexity at O(n), or 7n - 6 including the created x vector above. 
% For every kth iteration there are only 12 flops. There are
% a total of n-1 iteratins so this algorithm performs a total of 12n - 12
% flops which is O(n).

for k = 1 : n - 1
    
    for i = k + 1 : k + 2
        
        % Simple guard to check for possible out of bounds error
        if i > n
            break;
        end
        
        p = i - k; % Used remember the row that is currently being operated on

        % Calculates L_ik depending on the row currently being operated on
        if p == 1
            ld1(k) = ld1(k) / cd(k);
        else 
            ld2(k) = ld2(k) / cd(k);
        end
        
        for j = k + 1 : k + 2
            
            % Simple guard to check for possible out of bounds error
            if j > n
                break;
            end
            
            % Calculates a_ij depending on the row and column currently being operated on
            if i == j && p == 1 % i.e. in the main diagonal and upper row
                cd(j) = cd(j) - ld1(k) * ud1(k);        
            elseif i == j % i.e. in the main diagonal and lower row
                cd(j) = cd(j) - ld2(k) * ud2(k);
            elseif i < j % i.e. in upper row
                ud1(j-1) = ud1(j-1) - ld1(k) * ud2(k);
            else % i.e. in lower row
                ld1(j) = ld1(j) - ld2(k) * ud1(k);
            end 
                
        end
    end
end

% The following routine calculates Ly = b. To save space the components of
% b are simply rewritten with the corresponding newly calculated component
% of y. b(1) is left unchanged since y(1) = b(1). This routing performs a
% total of 4(n-3) + 2 flops.

b(2) = b(2) - ld1(1) * b(1);
 
for k = 3 : n
     b(k) = b(k) - ld1(k-1) * b(k-1) - ld2(k-2) * b(k-2);
end

% The following routine calculates Ux = y. This routine performs a total of
% 5(n-2) + 4 flops.
x(n) = b(n) / cd(n);
x(n-1) = (b(n-1) - ud1(n-1) * x(n)) / cd(n-1);
  
for k = n - 2 : -1 : 1 
     x(k) = (b(k) - ud1(k) * x(k+1) - ud2(k) * x(k+2)) / cd(k);
end

% In total, entire algorithm has space complexity of O(n) and 
% performs O(n) flops

end



