format('long','g');

x = zeros(10000,1); % Creates space for x_exact

cd = zeros(10000,1); % Creates space for central diagonal

ld1 = zeros(9999, 1); % Creates space for first lower diagonal

ld2 = zeros(9998, 1); % Creates space for second lower diagonal

ud1 = zeros(9999, 1); % Creates space for first upper diagonal

ud2 = zeros(9998, 1); % Creates space for second upper diagonal

% This loop fills in both the central diagonal and x_exact with the
% required values
for i = 1 : 10000 
    cd(i) = 8 * i;
    x(i) = 1;
end

% This loop fills in both the first lower and upper diagonal with the
% required values
for i = 1 : 9999 
    ld1(i) = -2 * i;
    ud1(i) = -2 * i;
end

% This loop fills in both the second lower and upper diagonals with the
% required values
for i = 1 : 9998
    ld2(i) = i;
    ud2(i) = i;
end

b = zeros(10000,1); % Creates the space for b

% The following code calculates b for Ax_exact = b
b(1) = cd(1) + ud1(1) + ud2(1);
b(2) = cd(2) + ud1(2) + ud2(2) + ld1(1);
b(9999) = cd(9999) + ud1(9999) + ld1(9998) + ld2(9997);
b(10000) = cd(10000) + ld1(9999) + ld2(9998);

for i = 3 : 9998 
    b(i) = cd(i) + ud1(i) + ud2(i) + ld1(i-1) + ld2(i-2);
end

% Run function to solve Au = b
u = pentadiagonal(ld2, ld1, cd, ud1, ud2, b);

% Calculate 2_norm of x_exact-u 
n = norm(x-u);

% Display result of 2_norm calculaton
disp(n);